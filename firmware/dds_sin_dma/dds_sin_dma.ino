#define NS  128
 
uint32_t Wave_LUT[NS] = {
    2048, 2149, 2250, 2350, 2450, 2549, 2646, 2742, 2837, 2929, 3020, 3108, 3193, 3275, 3355,
    3431, 3504, 3574, 3639, 3701, 3759, 3812, 3861, 3906, 3946, 3982, 4013, 4039, 4060, 4076,
    4087, 4094, 4095, 4091, 4082, 4069, 4050, 4026, 3998, 3965, 3927, 3884, 3837, 3786, 3730,
    3671, 3607, 3539, 3468, 3394, 3316, 3235, 3151, 3064, 2975, 2883, 2790, 2695, 2598, 2500,
    2400, 2300, 2199, 2098, 1997, 1896, 1795, 1695, 1595, 1497, 1400, 1305, 1212, 1120, 1031,
    944, 860, 779, 701, 627, 556, 488, 424, 365, 309, 258, 211, 168, 130, 97,
    69, 45, 26, 13, 4, 0, 1, 8, 19, 35, 56, 82, 113, 149, 189,
    234, 283, 336, 394, 456, 521, 591, 664, 740, 820, 902, 987, 1075, 1166, 1258,
    1353, 1449, 1546, 1645, 1745, 1845, 1946, 2047
};
 
DAC_HandleTypeDef hdac1;
TIM_HandleTypeDef htim2;
DMA_HandleTypeDef hdma_dac1;

void setup() {
  // Initialize the HAL Library
  HAL_Init();

  // Configure the system clock
  SystemClock_Config();

  // Initialize DAC1
  DAC_ChannelConfTypeDef sConfig = {};
  hdac1.Instance = DAC1;
  HAL_DAC_Init(&hdac1);

  sConfig.DAC_Trigger = DAC_TRIGGER_T2_TRGO;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_1);

  // Initialize Timer 2
  htim2.Instance = TIM2;
  htim2.Init.Period = 1; // Adjust as necessary
  htim2.Init.Prescaler = 0; // Adjust as necessary
  htim2.Init.ClockDivision = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  HAL_TIM_Base_Init(&htim2);

  // Configure TIM2 TRGO as the trigger for DAC
  TIM_MasterConfigTypeDef sMasterConfig = {};
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig);

  // Initialize DMA1 for DAC1
  hdma_dac1.Instance = DMA1_Stream5;
  hdma_dac1.Init.Channel  = DMA_CHANNEL_7;
  hdma_dac1.Init.Direction = DMA_MEMORY_TO_PERIPH;
  hdma_dac1.Init.PeriphInc = DMA_PINC_DISABLE;
  hdma_dac1.Init.MemInc = DMA_MINC_ENABLE;
  hdma_dac1.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
  hdma_dac1.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
  hdma_dac1.Init.Mode = DMA_CIRCULAR;
  hdma_dac1.Init.Priority = DMA_PRIORITY_HIGH;
  hdma_dac1.Init.FIFOMode = DMA_FIFOMODE_DISABLE;         
  HAL_DMA_Init(&hdma_dac1);

  __HAL_LINKDMA(&hdac1, DMA_Handle1, hdma_dac1);

  // Start DAC with DMA
  HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, (uint32_t*)Wave_LUT, 128, DAC_ALIGN_12B_R);

  // Start Timer 2
  HAL_TIM_Base_Start(&htim2);
}

void loop() {
  // Main loop code
}

// Define SystemClock_Config() according to your system's clock configuration
void SystemClock_Config() {
  // System Clock Configuration Code
}
